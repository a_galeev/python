#!/usr/bin/env python

# credit_cards1 (������������)

# �������� ��������� ��� ������������� � �������� 
# ���������� ������� ��������� ���� ��� �����:
#   - Visa 13, 16 ��� 19 ����
#   - MasterCard 16 ����
#   - AmericanExpress 15 ����

# ��� 1. ���������� ������� calc_luhn

import pytest

def calc_luhn(n):
    # ��������� �� ���� �����.
    # ��������� ������� �� ������� �� 10 ����������� �����,
    # ���������� �� ��������� ����: 
    #   - �������� �����, ������ ������, ������ ���������
    #   - ������ �����, ������ ������, �������� �� 2, 
    # ��������� ��������� ����� ������������ �����
    #   - ��������� ��� ��� ����������.
    # ��������:
    # n = '2  3  6  7  8'
    #        x2    x2 
    # 8 + 6 + 2 + (1 + 4) + 6 = 27
    # res = 7
    n = str(n)
    res = 0
    for i, item in enumerate(reversed([int(x) for x in n])):
        if i%2 != 0:
            item *= 2
            if item > 9:
                item -= 9
        res += item
    return res % 10
    #print(res % 10)
    #print(sec)



def test_calc_luhn():
    assert calc_luhn(1) == 1
    assert calc_luhn(23) == 7
    assert calc_luhn(23678) == 7

if __name__ == '__main__':
    # ��� ����� ������� ������ ������ assert ������ ������ ��/��� �����
    # �������� ����� ��������� ����������, ���� ���-�� ����� �� ���.
    pytest.main([__file__])
#    pytest.main(['__file__ + '::test3'])    # ��������� ������ ������ ����
#    pytest.main(['-s', __file__ + '::test3'])   # �� �� + ����������� ������� ipdb


    #calc_luhn(1)
    #calc_luhn(23)
    #calc_luhn(23678)