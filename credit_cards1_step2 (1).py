#!/usr/bin/env python

# credit_cards1 (обязательная)
#
# Напишите программу для генерирования и проверки 
# валидности номеров кредитных карт трёх видов:
#   - Visa 13, 16 или 19 цифр
#   - MasterCard 16 цифр
#   - AmericanExpress 15 цифр

# Шаг 2. Реализуйте класс Visa, как описано ниже

import pytest
from random import randint, choice

def calc_luhn(n):
    n = str(n)
    res = 0
    for i, item in enumerate(reversed([int(x) for x in n])):
        if i%2 != 0:
            item *= 2
            if item > 9:
                item -= 9
        res += item
    return res % 10

class Visa():
    def __init__(self, number):
        # Принимает на вход строку, избавляется от пробелов,
        # преобразует в число, сохраняет в поле класса
        self.number = str(''.join(str(number).split(' ')))
        self.listoflens = [13,16,19]

    def is_valid(self):
        # Проверяет длину строки (должно быть 13, 16 или 19) и 
        # код Луна (должен быть 0)
        if len(self.number) in self.listoflens:
            if calc_luhn(self.number) == 0:
                return True
            else:
                return False
        else:
            return False

    @staticmethod
    def generate():
        # Случайным образом выбирается длина, затем случайным же 
        # образом генерируются цифры так, чтобы «сошёлся» код Луна.
        # Возвращается объект класса Visa с полученным номером.
        # Рекомендуется использовать choice и randint из random.
        # @staticmethod - это так называемый декоратор, он делает метод
        # статическим, такой метод можно вызывать на классе: Visa.generate(), 
        # а не только на объекте класса: Visa().generate().
        # Используется, например, для альтернативных конструкторов (как здесь).
        listoflens = [13,16,19]
        gen_len = choice(listoflens)
        degree = gen_len - 2
        gen_num = randint(10**degree, 10**(degree + 1) - 1)
        print(len(str(gen_num)))
        visa_number = str(gen_num) + str(0)
        if calc_luhn(visa_number) != 0:
            visa_number = str(gen_num) + str(10 - calc_luhn(visa_number))
        return visa_number

    def __str__(self):
        # Возвращает номер карты в виде строки
        return self.number

def test_calc_luhn():
    assert calc_luhn(1) == 1
    assert calc_luhn(23) == 7
    assert calc_luhn(23678) == 7

def test_str():  assert Visa('1234 5678').number == '12345678'

def test0():  assert Visa('').is_valid() == False
def test1():  assert Visa('23678').is_valid() == False
def test2():  assert Visa('1234 5678 9012 3456').is_valid() == False
def test3():  assert Visa('4929 5958 3592 5180').is_valid() == True
def test4():  assert Visa('4929 5958 3592 5181').is_valid() == False

def test8():
    # Генерирует номер карты и проверяет его валидность
    for i in range(1000):
        card = Visa.generate()
        assert Visa(card).is_valid() == True

if __name__ == '__main__':
    # При таком способе вызова каждый assert вместо просто да/нет будет
    # выдавать более детальную информацию, если что-то пошло не так.
    pytest.main([__file__])
#    pytest.main(['__file__ + '::test8'])    # запускает только третий тест
#    pytest.main(['-s', __file__ + '::test3'])   # то же + возможность отладки ipdb
    # for i in range(10):
    #     card = Visa.generate()
    #     print(card, len(card), calc_luhn(card))
    #     if Visa(card).is_valid() == True:    
    #         print("asd")
    #         #pass
    #     else:
    #         break
    # print(i)
    # print(Visa('4929 5958 35925180').is_valid())
    #print("asd", Visa.genereate())
