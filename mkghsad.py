#!/usr/bin/env python

# credit_cards1 (обязательная)
#
# Напишите программу для генерирования и проверки 
# валидности номеров кредитных карт трёх видов:
#   - Visa 13, 16 или 19 цифр
#   - MasterCard 16 цифр
#   - AmericanExpress 15 цифр

# Шаг 3. Реализуйте класс Mastercard, как описано ниже

import pytest
from random import randint, choice


def calc_luhn(n):
    n = str(n)
    res = 0
    for i, item in enumerate(reversed([int(x) for x in n])):
        if i%2 != 0:
            item *= 2
            if item > 9:
                item -= 9
        res += item
    return res % 10

class CreditCard:
    # Выделите общую функциональность для классов Visa и MasterCard
    # в базовый класс CreditCard, оставив особенности в дочерних
    # классах.
    def __init__(self, number):
        self.number = str(''.join(str(number).split(' ')))

    def is_valid(self):
        if len(self.number) in self.length:
            if calc_luhn(self.number) == 0:
                return True
            else:
                return False
        else:
            return False

    @classmethod
    def generate(cls):
        # @classmethod - это декоратор, который который превращает метод
        # в нечто среднее между обычным и статическим методом.
        # В отличие от статического, classmethod имеет доступ 
        # к полям класса (но не объекта).
        gen_len = choice(cls.length)
        degree = gen_len - 2
        gen_num = randint(10**degree, 10**(degree + 1) - 1)
        #print(len(str(gen_num)))
        card_number = str(gen_num) + str(0)
        if calc_luhn(card_number) != 0:
            card_number = str(gen_num) + str(10 - calc_luhn(card_number))
        return cls(card_number)

class Visa(CreditCard):
    length = [13,16,19]

class MasterCard(CreditCard):
    length = [16]

def test_calc_luhn():
    assert calc_luhn(1) == 1
    assert calc_luhn(23) == 7
    assert calc_luhn(23678) == 7

def test_str():  assert Visa('1234 5678').number == '12345678'

def test0():  assert Visa('').is_valid() == False
def test1():  assert Visa('23678').is_valid() == False
def test2():  assert Visa('1234 5678 9012 3456').is_valid() == False
def test3():  assert Visa('4929 5958 3592 5180').is_valid() == True
def test4():  assert Visa('4929 5958 3592 5181').is_valid() == False
def test5():  assert MasterCard('5578 2350 9610 0287').is_valid() == True

def test8():
    # Генерирует номер карты и проверяет его валидность



    # for i in range(1000):
    #     card = Visa.generate()
    #     assert card.is_valid() == True


    for i in range(1000):
        card = Visa.generate()
        card_MC = MasterCard.generate()
        assert card.is_valid() == True
        assert card_MC.is_valid() == (len(card_MC.number) == 16)

if __name__ == '__main__':
    # При таком способе вызова каждый assert вместо просто да/нет будет
    # выдавать более детальную информацию, если что-то пошло не так.
    pytest.main([__file__])
#    pytest.main(['__file__ + '::test3'])    # запускает только третий тест
#    pytest.main(['-s', __file__ + '::test3'])   # то же + возможность отладки ipdb
    print("asd")


